﻿using HtmlAgilityPack;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using ZapLib;

namespace PTT_Crawler
{
    class Program
    {
        const string Base = "https://www.ptt.cc"; // host base      
        static int MAX_FETCHING_PAGE = 3; // how many pages we want to get

        static void Main(string[] args)
        {

            // get init page
            HtmlNode node = LoadPage("https://www.ptt.cc/bbs/C_Chat/index1.html");
            if (node == null) Exit();

            // all of urls
            List<string> urls = new List<string>();

            // get urls 
            while (MAX_FETCHING_PAGE > 1)
            {
                urls.AddRange(CollectArticlesURL(node));
                node = GetNextPage(node);
                MAX_FETCHING_PAGE -= 1;
            }
            // get each page
            List<ModelArticle> data = new List<ModelArticle>();
            foreach (var u in urls)
            {
                ModelArticle article = GetPageData(u);
                if (article != null) data.Add(article);
            }

            // save as JSON
            File.WriteAllText("data.json", JsonConvert.SerializeObject(data));
            Exit();
        }



        /*
            get article data
        */
        static ModelArticle GetPageData(string url)
        {
            // create ArticleModel and load article page
            ModelArticle info = new ModelArticle();
            HtmlNode node = LoadPage(url);
            if (node == null) return null;

            // get meta data
            info.url = url;
            var metas = node.SelectNodes("//span[@class=\"article-meta-value\"]");
            if (metas.Count < 4) return null;
            info.author = metas[0].InnerText;
            info.board = metas[1].InnerText;
            info.title = metas[2].InnerText;
            info.datetime = metas[3].InnerText;

            // collect all of contents
            string content = "";
            var tmp_nodes = node.SelectNodes("//div[@class=\"article-metaline\"]");
            var tmp_node = tmp_nodes[tmp_nodes.Count - 1].SelectSingleNode("following-sibling::node()");
            while (tmp_node != null)
            {
                if (isClassName(tmp_node, "f2")) break;
                content += "\n" + tmp_node.InnerText.Trim();
                tmp_node = tmp_node.SelectSingleNode("following-sibling::node()");
            }
            info.content = content;

            // collect comments and rely
            tmp_node = node.SelectSingleNode("//div[@class=\"push\"]");
            while (tmp_node != null)
            {
                // collect comment metadata
                ModelComment c = new ModelComment();
                var comment_node = tmp_node.SelectNodes("*");
                if (comment_node.Count < 4) continue;
                c.score = comment_node[0].InnerText.Trim();
                c.author = comment_node[1].InnerText.Trim();
                c.content = comment_node[2].InnerText.Trim();
                c.datetime = comment_node[3].InnerText.Trim();

                // get sibling node until next "push" tag
                tmp_node = tmp_node.SelectSingleNode("following-sibling::node()");
                while (!isClassName(tmp_node, "push") && tmp_node != null)
                {
                    c.reply += tmp_node.InnerText.Trim();
                    tmp_node = tmp_node.SelectSingleNode("following-sibling::node()");
                }
                info.comments.Add(c);
            }
            return info;
        }

        /*
            collect all of article url list
        */
        static List<string> CollectArticlesURL(HtmlNode node)
        {
            List<string> urls = new List<string>();
            foreach (var n in node.SelectNodes("//div[@class=\"title\"]/a"))
                urls.Add(Base + n.Attributes["href"].Value);
            return urls;
        }

        /*
            get next page
        */
        static HtmlNode GetNextPage(HtmlNode node)
        {
            var btns = node.SelectNodes("//div[@class=\"btn-group btn-group-paging\"]//a");
            if (btns.Count < 4) return null;
            if (btns[2].Attributes["href"] == null) return null;
            return LoadPage(Base + btns[2].Attributes["href"].Value);
        }

        /*
            compare ndoe classname 
        */
        static bool isClassName(HtmlNode node, string className)
        {
            if (node == null) return false;
            var class_attr = node.Attributes["class"];
            if (class_attr != null)
                if (class_attr.Value == className) return true;
            return false;
        }

        /*
            load page using url and return HtmlNode
        */
        static HtmlNode LoadPage(string url)
        {
            Fetch f = new Fetch(url);
            string html = f.get(null);
            if (html == null)
            {
                Console.WriteLine("can not load html: " + f.getResponse());
                return null;
            }
            else
            {
                HtmlDocument Doc = new HtmlDocument();
                Doc.LoadHtml(html);
                return Doc.DocumentNode;
            }
        }

        /*
            Exit process 
        */
        static void Exit(string error = "按任意鍵結束...")
        {
            Console.WriteLine(error);
            Console.ReadKey();
            Environment.Exit(0);
        }
    }

    class ModelArticle
    {
        public string author { get; set; }
        public string title { get; set; }
        public string board { get; set; }
        public string datetime { get; set; }
        public string content { get; set; }
        public string url { get; set; }
        public List<ModelComment> comments { get; set; } = new List<ModelComment>();
    }
    class ModelComment
    {
        public string score { get; set; }
        public string author { get; set; }
        public string content { get; set; } = "";
        public string datetime { get; set; }
        public string reply { get; set; } = "";
    }
}
