# PTT Crawler

基於教育訓練的成果，抓取 PTT [C_Chat](https://www.ptt.cc/bbs/C_Chat/) 看板的所有文章

## Launch

```
git clone http://192.168.1.136/zap/PTT_Crawler.git
```
* 啟動 VS 
* 開啟方案 PTT_Crawler
* 直接執行即可

## 說明

為了避免疑似暴力攻擊，故在程式碼加上 `MAX_FETCHING_PAGE` 參數，以限制抓取幾個頁面的所有文章，預設抓 3 頁

### 文章結構

使用 `ModelArticle` 類別儲存

* `author`: 文章作者
* `title`: 文章標題
* `board`: 文章所在看板
* `datetime`: 文章發布時間
* `content`: 文章內容
* `url`: 原始位置
* `comments`: 所有留言 (留言結構參閱 `ModelComment`)

### 留言結構

使用 `ModelComment` 類別儲存

* `score`: 標示推或噓的內容
* `author`: 文章作者
* `content`: 文章內容
* `datetime`: 文章發布時間
* `reply`: 作者的回覆

## 輸出

使用 JSON.NET 套件將模型序列化後輸出，當然可以自行修改輸出方式，例如 `Console.Write` 印出

```json

 {
    "author": "dionysus7788 (低等動物               )",
    "title": "[塗鴉] 希洽&amp;�堿◥熔蚰�",
    "board": "C_Chat",
    "datetime": "Tue Dec  9 00:44:59 2008",
    "content": "\n有人反應看不到圖 故補上相簿圖 應該不會看不到了\n\n希洽\nhttp://i716.photobucket.com/albums/ww167/dionysus7788/2.jpg\n\n\n\nhttp://www.wretch.cc/album/show.php?i=dionysus7788&amp;b=2&amp;f=1441294510&amp;p=31\n�堿�(舊圖上色)\nhttp://i716.photobucket.com/albums/ww167/dionysus7788/3.jpg\n\n\n\nhttp://www.wretch.cc/album/show.php?i=dionysus7788&amp;b=2&amp;f=1441294508&amp;p=29\n「跟我搶男人？我就讓妳永遠沒臉見人...！」\n\n「不過...沒想到這麼好賺呢，不過一天而已...」\n\n對不起 雖然已經有人反應好久沒看到賣萌的希洽\n\n可是這張希洽還是壞掉了= =\n\n下一張會正常一點吧(應該吧XD)\n\n--\n  18166    4/01 -    □ (本文\n已\n被\n刪\n除)",
    "url": "https://www.ptt.cc/bbs/C_Chat/M.1228754707.A.344.html",
    "comments": [
        {
            "score": "推",
            "author": "finaltrial",
            "content": ":幾霸婚",
            "datetime": "12/09 00:47",
            "reply": ""
        },
        {
            "score": "→",
            "author": "ttoy",
            "content": ":我推~~",
            "datetime": "12/09 00:48",
            "reply": ""
        },
        {
            "score": "推",
            "author": "ambitionjojo",
            "content": ":撒霸婚",
            "datetime": "12/09 00:48",
            "reply": ""
        },
        {
            "score": "推",
            "author": "sandwichpope",
            "content": ":1000000000000000000000000000000000000000000000000",
            "datetime": "12/09 00:49",
            "reply": ""
        },
	...
```

## License MIT


	Copyright (C) 2018 ZapLin
	Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
	The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
